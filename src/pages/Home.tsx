import { Box, HStack, Image, Link, Text } from '@chakra-ui/react';
import AG_Leventis from '../assets/AGLeventis_Logo.jpg';
import MetaMobilityLogo from '../assets/MetaLogo.png';
import backgroundImg from '../assets/background.jpg';
import CyprusInstitute from '../assets/cyprus-institute.png';
import David_prize from '../assets/david_prize.png';
import RIF from '../assets/logos.png';

export const Home = () => {
  return (
    <Box
      maxW="100xl"
      backgroundColor={'#111312'}
      backgroundImage={`url(${backgroundImg})`}
      backgroundSize="cover"
      backgroundPosition="center"
    >
      <Box backgroundColor="rgba(0,0,0,0.3)" width="100%" p={4}>
        <Text
          fontWeight="bold"
          fontSize="6xl"
          color="#e9a542"
          fontFamily="Raleway"
          mb="4"
        >
          Welcome to ZooBi(bli)oArch!
        </Text>
        <Text
          fontSize="2xl"
          fontWeight="semibold"
          color="#e9a542"
          fontFamily="Raleway"
          mb="2"
        >
          Bibliographic database for zooarchaeological studies in the Eastern
          Mediterranean and Middle East
        </Text>
        <Text
          textAlign="justify"
          fontWeight="semibold"
          fontFamily="Raleway"
          color="#e9e9e9"
        >
          ZooBi(bli)oArch is a bibliographic database focusing on studies of
          faunal archaeological remains in the Eastern Mediterranean and Middle
          East; it covers assemblages from prehistory to early modern times and
          is an extension of the Bi(bli)oArch initiative
          (https://www.biblioarch.com/). ZooBi(bli)oArch offers a range of
          search criteria and filters, allowing users to refine their search
          based on research topic, region, or publication date. It emerged from
          an increasing realization that many studies on faunal archaeological
          remains have been disseminated across national archaeology journals,
          excavation monographs, 'grey' reports, or in the form of graduate
          theses, limiting their discoverability. This tool currently contains
          3,668 titles and will be continuously updated. Our team is looking to
          expand the database through community-based contributions. Additional
          papers, corrections and/or omissions can be submitted through our
          submissions/feedback form or by emailing us directly. This database
          was created in the context of the MetaMobility project. This project
          is implemented under the programme of social cohesion “THALIA
          2021-2027” cofunded by the European Union, through Research and
          Innovation Foundation (EXCELLENCE/0421/0376). Database creation was
          also supported by the Dan David Prize and the A. G. Leventis Chair in
          Archaeological Sciences.
        </Text>
        <HStack borderRadius={20} justifyContent="space-between" wrap="wrap">
          <Image
            src={MetaMobilityLogo}
            alt="MetaMobility logo"
            h="auto"
            w="240px"
          />
          <Image
            src={David_prize}
            alt="Dan David Prize logo"
            h="auto"
            w="230px"
          />
          <Image src={AG_Leventis} alt="AG Leventis logo" h="auto" w="100px" />
          <Link href="https://www.cyi.ac.cy/">
            <Image
              src={CyprusInstitute}
              w="115px"
              h="auto"
              alt="Cyprus Institute Logo"
            />
          </Link>
          <Image src={RIF} alt="RIF Logo" />
        </HStack>
      </Box>
    </Box>
  );
};
