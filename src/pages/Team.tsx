import { Avatar, Flex, Text } from '@chakra-ui/react';
import team1 from '../assets/team1.jpg';
import team2 from '../assets/team2.jpg';
import team3 from '../assets/team3.jpg';
import team4 from '../assets/team4.jpg';
import team5 from '../assets/team5.jpg';
import team6 from '../assets/team6.jpg';

export const Team = () => {
  return (
    <Flex direction="column" mx="200" my="20" gap={4} color="white">
      <Member name="Mahmoud Mardini" src={team1}>
        <Text fontSize="smaller" textAlign="justify">
          Dr Mahmoud Mardini joined the Cyprus Institute as a PhD student in
          October 2019 after receiving his MSc degree in Osteoarchaeology at the
          University of Sheffield (United Kingdom, 2017) and his BA degree in
          Archaeology at the American University of Beirut (Lebanon, 2016). He
          completed his PhD in Science and Technology in Archaeology and
          Cultural Heritage in December 2023. As an early-career researcher,
          Mahmoud has extensive archaeological fieldwork experience, including
          over twelve excavations and four surveys in Lebanon. Shortly after his
          MSc and during his time at the University of Sheffield, Mahmoud gained
          laboratory and teaching experience by working as a laboratory
          assistant and demonstrator for a number of short courses and one MSc
          module. As an osteoarchaeologist, Mahmoud has worked with the Lebanese
          Department of Antiquities and the American University of Beirut on
          various human bone assemblages from Beirut, Byblos, Chekka, and
          Baalbek; temporally covering the Middle-Bronze Age to the Roman
          Period. In addition, as a zooarchaeologist, he has analysed two faunal
          assemblages from Beirut and Jabal Moussa. Mahmoud’s PhD project
          performed a biocultural study on life quality and mobility in
          different populations across Lebanon during the classical era of Roman
          ‘Phoenicia’. His work has been disseminated in dozens of publications.
        </Text>
      </Member>
      <Member name="Efthymia Nikita" src={team2}>
        <Text fontSize="smaller" textAlign="justify">
          Dr Efthymia Nikita is Associate Professor in Bioarchaeology at the
          Science and Technology in Archaeology and Culture Research Center
          (STARC) of The Cyprus Institute. She studies human skeletal remains
          across the Mediterranean from prehistoric to post-Medieval contexts,
          shedding light on health and disease, diet, activity, demography and
          other important aspects of life in the past. She earned a BA in
          Archaeology from the Aristotle University of Thessaloniki and a PhD in
          Biological Anthropology from the University of Cambridge. Prior to her
          appointment at STARC, she held post-doctoral posts at the British
          School at Athens and the American School of Classical Studies at
          Athens, as well as a Marie Skłodowska-Curie fellowship at the
          University of Sheffield. Her research has resulted in over 100
          articles and book chapters, including the textbook ‘Osteoarchaeology:
          A Guide to the Macroscopic Study of Human Skeletal Remains’ (Elsevier
          2017). She is currently the co-Editor of the Journal of Archaeological
          Science and she recently also co-Edited the second edition of the
          Encyclopedia of Archaeology (Elsevier). In 2022, she was awarded the
          Dan David prize for her contribution to the study of the past.
        </Text>
      </Member>

      <Member name="Angelos Hadjikoumis" src={team3}>
        <Text fontSize="smaller" textAlign="justify">
          Dr Angelos Hadjikoumis is a zooarchaeologist. His research mainly
          involves the zooarchaeology of eastern Mediterranean and adjacent
          areas such as the Aegean and Mesopotamia, as well as the
          zooarchaeology of Iberia and Britain. Thematically, his interests
          currently focus on sheep and goat management, the use of animals in
          sacrifices/rituals, as well as animal husbandry practices from the
          Neolithic to post-medieval periods. Besides zooarchaeological methods,
          he also uses carbon/oxygen stable isotope and dental microwear
          analyses to explore past animal management. He is also particularly
          active in the ethnozooarchaeology of traditional Mediterranean
          husbandry practices as a heuristic tool in zooarchaeological
          interpretation. Dr Hadjikoumis is also an enthusiast of public
          outreach and the creation and management of faunal reference
          collections, as well as improving the teaching of zooarchaeology.
        </Text>
      </Member>

      <Member name="Mohamad Mardini" src={team4}>
        <Text fontSize="smaller" textAlign="justify">
          Software Developer
        </Text>
      </Member>

      <Member name="Ledia Lato" src={team5}>
        <Text fontSize="smaller" textAlign="justify">
          Intern in Bioarchaeology
        </Text>
      </Member>

      <Member name="Stella Polyzou" src={team6}>
        <Text fontSize="smaller" textAlign="justify">
          Intern in Bioarchaeology
        </Text>
      </Member>
    </Flex>
  );
};

interface MemberInfo {
  name: string;
  src: string;
  children: any;
}

const Member: React.FC<MemberInfo> = ({ name, src, children }) => {
  return (
    <Flex gap={4}>
      <Avatar size="2xl" name={name} src={src} />
      <Flex gap={4} direction="column">
        <Text color="#e9a542" fontSize="2xl">
          {name}
        </Text>
        <Text>{children}</Text>
      </Flex>
    </Flex>
  );
};
