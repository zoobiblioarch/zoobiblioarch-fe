import { Button, Flex, Text } from '@chakra-ui/react';
import { Paper } from '../store/PaperModel';

interface DataItemProps {
  paper: Paper;
  addOrRemoveFromExports: (paper: Paper) => void;
  isInExports: (paper: Paper) => boolean;
}

export const DataItem: React.FC<DataItemProps> = ({
  paper,
  addOrRemoveFromExports,
  isInExports,
}) => {
  return (
    <Flex
      padding={6}
      border="2px solid"
      borderColor="#e9a542"
      borderRadius={6}
      wrap="wrap"
      gap={5}
      flexWrap="wrap"
      minH={100}
    >
      <Item label="Title" text={paper.title} />
      <Item label="Authors" text={paper.authors} />
      {/* <Item
        label="Chronology"
        text={paper.chronology.map((chronology) => chronology.name).join(', ')}
      /> */}
      <Item label="Year" text={paper.year as unknown as string} />
      <Item
        label="Keywords"
        text={paper.keywords.map((keyword) => keyword.name).join(', ')}
      />
      <Item
        label="Region"
        text={paper.regions.map((region) => region.name).join(', ')}
      />
      <Item label="DOI" text={paper.doi} />
      <Item label="Journal/Book" text={paper.journal_book} />
      <Item label="Abstract" text={paper.abstract} />
      <Button
        bg={isInExports(paper) ? '#e9a542' : 'gray.200'}
        onClick={() => {
          addOrRemoveFromExports(paper);
        }}
      >
        {isInExports(paper) ? 'Remove From' : 'Add To'} Exports
      </Button>
    </Flex>
  );
};
interface ItemProps {
  label: string;
  text: string;
}
const Item: React.FC<ItemProps> = ({ label, text }) => (
  <Flex gap={1}>
    <Text color="#e9a542" fontWeight="bold">
      {label}:{' '}
    </Text>
    <Text color="#FFF">{text}</Text>
  </Flex>
);
