import {
  Flex,
  Menu,
  MenuButton,
  Button,
  MenuList,
  Input,
  Spinner,
  MenuOptionGroup,
  MenuItemOption,
  MenuItem,
} from '@chakra-ui/react';
import React from 'react';
import { Paper } from '../store/PaperModel';
import { PaperStore } from '../store/PaperStore';

interface PaginationProps {
  currentPage: number;
  hasNext: boolean;
  hasPrev: boolean;
  totalPages: number;
  totalDocs: number;
  isLoading: boolean;
  exports: Paper[];
  setSelectedFilters: PaperStore['setSelectedFilters'];
  fetchPapersByFilterCriteria: PaperStore['fetchPapersByFilterCriteria'];
  generateTSV: (exports?: Paper[]) => void;
  generatePDF: (exports?: Paper[]) => void;
  generateJSON: (exports?: Paper[]) => void;
  clearExports: () => void;
}

export const Pagination: React.FC<PaginationProps> = ({
  currentPage,
  hasPrev,
  hasNext,
  totalDocs,
  totalPages,
  isLoading,
  exports,
  setSelectedFilters,
  fetchPapersByFilterCriteria,
  generateJSON,
  generatePDF,
  generateTSV,
  clearExports,
}) => {
  return (
    <Flex gap={2} justifyContent="space-between">
      <Menu>
        <MenuButton disabled={isLoading} as={Button}>
          Results Per Page
        </MenuButton>
        <MenuList>
          <MenuOptionGroup
            defaultValue="10"
            onChange={(value) => {
              setSelectedFilters({ pageSize: Number(value) });
              fetchPapersByFilterCriteria();
            }}
          >
            <MenuItemOption value="10">10</MenuItemOption>
            <MenuItemOption value="20">20</MenuItemOption>
            <MenuItemOption value="50">50</MenuItemOption>
            <MenuItemOption value="100">100</MenuItemOption>
            <MenuItemOption value="500">500</MenuItemOption>
          </MenuOptionGroup>
        </MenuList>
      </Menu>
      <Menu>
        <MenuButton disabled={isLoading} as={Button}>
          Export Filtered Data
        </MenuButton>
        <MenuList>
          <MenuItem onClick={() => generateTSV()}>TSV</MenuItem>
          <MenuItem onClick={() => generatePDF()}>PDF</MenuItem>
          <MenuItem onClick={() => generateJSON()}>RAW(JSON)</MenuItem>
        </MenuList>
      </Menu>
      <Menu>
        <MenuButton disabled={isLoading} as={Button}>
          Export Selected Data
        </MenuButton>
        <MenuList>
          <MenuItem onClick={() => generateTSV(exports)}>TSV</MenuItem>
          <MenuItem onClick={() => generatePDF(exports)}>PDF</MenuItem>
          <MenuItem onClick={() => generateJSON(exports)}>RAW(JSON)</MenuItem>
        </MenuList>
      </Menu>
      <Button onClick={clearExports}>Clear Selected Items</Button>
      <Flex gap={2} alignItems="center">
        <Button
          disabled={!hasPrev || isLoading}
          onClick={() => {
            setSelectedFilters({ page: 0 });
            fetchPapersByFilterCriteria();
          }}
        >
          {'<<'}
        </Button>
        <Button
          disabled={!hasPrev || isLoading}
          onClick={() => {
            setSelectedFilters({ page: currentPage - 1 });
            fetchPapersByFilterCriteria();
          }}
        >
          {'<'}
        </Button>
        <Flex color="#e9a542" fontWeight="bold">
          {isLoading ? <Spinner size="sm" /> : currentPage}/
          {isLoading ? <Spinner size="sm" /> : totalPages}
        </Flex>
        <Button
          disabled={!hasNext || isLoading}
          onClick={() => {
            setSelectedFilters({ page: currentPage + 1 });
            fetchPapersByFilterCriteria();
          }}
        >
          {'>'}
        </Button>
        <Button
          disabled={!hasNext || isLoading}
          onClick={() => {
            setSelectedFilters({ page: totalPages });
            fetchPapersByFilterCriteria();
          }}
        >
          {'>>'}
        </Button>
        <Flex color="#e9a542">
          Total Documents: {isLoading ? <Spinner size="sm" /> : totalDocs}
        </Flex>
      </Flex>
      <Flex gap={2}>
        <Input
          disabled={isLoading}
          maxW={200}
          color="white"
          placeholder="Go to page..."
          onChange={(e) => {
            setSelectedFilters({ page: Number(e.target.value) });
          }}
        />
        <Button
          disabled={isLoading}
          onClick={() => {
            fetchPapersByFilterCriteria();
          }}
        >
          Go
        </Button>
      </Flex>
    </Flex>
  );
};
